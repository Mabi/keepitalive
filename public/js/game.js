let canvas, gameBtn, menu, ctx, clown;

/** Scale Factor*/
let imgMod = 1.5;

/** Shrink Factor */
let imgModFac = 0.005;

let spawnTime = 800;

let mouseX, mouseY;

let circles = [];
let circleColours = ['BLUE', 'RED', 'GREEN', 'PURPLE'];
let activeCircle;

let isDown = false;
let clownX, clownY;
let clownWidth, clownHeight;
let goodAudio, badAudio;

let feedMeWith;
let score = 0;

let spawnInterval;
let imgModInterval;

document.addEventListener('DOMContentLoaded', function (event) {

    goodAudio = new sound('src/good.mp3');
    badAudio = new sound('src/bad.mp3');
    canvas = document.getElementById('myCanvas');
    canvas.addEventListener('mousemove', mouseMove);
    canvas.addEventListener('mousedown', mouseDown);
    canvas.addEventListener('mouseup', mouseUp);
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    gameBtn = document.getElementById('startGameBtn');
    gameBtn.addEventListener('click', gameStart);

});

function gameStart(e) {

    menu = document.getElementById('flexContainer');
    menu.style.display = 'none';
    canvas.style.display = 'block';
    ctx = canvas.getContext('2d');
    clown = new Image();
    clown.src = 'src/clown.png';
    clown.onload = function () {

        /** Sets first FeedPoint */
        setNewFeedPoint();

        /** Start Drawing */
        draw();

        imgModInterval = setInterval(() => {
            imgMod -= imgModFac;
        }, 50);

        spawnInterval = setInterval(() => {

            let cirlceR = Math.floor(Math.random() * 100) + 5; // 5 - 100

            let circleX = Math.floor((Math.random() * window.innerWidth));

            if ((circleX + cirlceR) > (window.innerWidth / 2 - clownWidth)
                &&
                (circleX + cirlceR) < (window.innerWidth / 2 + clownWidth)) {
                if (Math.round(Math.random()) == 0)
                    circleX -= (clownWidth + cirlceR);
                else
                    circleX += (clownWidth + cirlceR);
            }


            let circleY = Math.floor(Math.random() * window.innerHeight) + 1;

            let circleColour = circleColours[Math.floor(Math.random() * circleColours.length)];
            addCircle(circleX, circleY, cirlceR, circleColour);
        }, spawnTime);
    }
}

function draw() {
    /** Clear Canvas */
    ctx.clearRect(0, 0, window.innerWidth, window.innerHeight);

    circles.forEach((circle) => {
        drawCircle(circle);
    });

    /** Clown */
    clownWidth = clown.width * imgMod;
    clownHeight = clown.height * imgMod;

    clownX = window.innerWidth / 2 - (clownWidth / 2);
    clownY = window.innerHeight / 2 - (clownHeight / 2);

    ctx.drawImage(clown,
        clownX,
        clownY,
        clownWidth,
        clownHeight
    );

    /** Draws Colour Text */
    drawText(feedMeWith.text, '50px Arial', feedMeWith.colour, window.innerWidth / 2 - getTextWidth(feedMeWith.text, '50px Arial') / 2);

    /**  Draws Score (Top Right) */
    drawText('SCORE: ' + score, '30px Arial', 'black',
        window.innerWidth - getTextWidth(
        'SCORE: ' + score, '30px Arial'
        ) - 40, 40);

    /**  Draws Coords (Top Right) */
    // drawText('X: ' + mouseX + ' Y: ' + mouseY,);

    if (imgMod >= 0) {
        window.requestAnimationFrame(draw);
    } else {
        // TODO leaderboard ...
        alert('GAME OVER -- YOUR SCORE: ' + score);
        resetGame();
    }
}

function resetGame() {
    location.reload();
    // TODO leaderboard ...
    // menu.style.display = 'block';
    // canvas.style.display = 'none';
    // imgMod = 1.5;
    // window.clearInterval(spawnInterval);
    // window.clearInterval(imgModInterval);
    // circles = {};
}

function drawText(txt = '', font = '30px Arial', colour = 'black', x = 100, y = 100) {
    ctx.font = font;
    ctx.fillStyle = colour;
    ctx.fillText(txt, x, y);
}

function drawCircle(circle) {
    ctx.beginPath();
    ctx.arc(circle.x, circle.y, circle.r, 0, 2 * Math.PI);
    ctx.fillStyle = circle.colour
    ctx.fill();
    ctx.stroke();
}

function addCircle(x, y, r, colour) {
    circles.push({x: x, y: y, r: r, colour: colour});
}

function mouseDown() {
    circles.forEach((circle, index) => {
        if (pointInCircle(circle.x, circle.y, mouseX, mouseY, circle.r)) {
            activeCircle = index;
            isDown = true;
        }
    });
}

function mouseUp() {
    isDown = false;
    activeCircle = null;
}

function mouseMove(e) {
    mouseX = e.clientX;
    mouseY = e.clientY;

    if (isDown) {
        circles[activeCircle].x = mouseX;
        circles[activeCircle].y = mouseY;

        if (pointOnClown()) {
            feedIt(); // feed IT hehe
        }
    }
}

function feedIt() {

    if (circles[activeCircle].colour != feedMeWith.text) {
        // WRONG BALL
        imgMod -= circles[activeCircle].r / 1000;
        score--;
        badAudio.play();
    } else {
        imgMod += circles[activeCircle].r / 1000;
        score++;
        goodAudio.play();
    }

    /** Delete Circle  */
    circles.splice(activeCircle, 1);
    isDown = false;
    activeCircle = null;

    setNewFeedPoint();
}

function setNewFeedPoint() {
    /** Set New FeedPoint */
    feedMeWith = {
        text: circleColours[Math.floor(Math.random() * circleColours.length)],
        colour: circleColours[Math.floor(Math.random() * circleColours.length)],
    };
}

/** Helper **/
function pointInCircle(x0, y0, x1, y1, r) {
    return Math.sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0)) < r;
}

function pointOnClown() {
    return (mouseX >= clownX && mouseX <= (clownX + clownWidth) && mouseY >= clownY && mouseY <= (clownY + clownHeight));
}

function getTextWidth(txt, font) {
    let c = getTextWidth.canvas || (getTextWidth.canvas = document.createElement('canvas'));
    let ctx = c.getContext('2d');
    ctx.font = font;
    let m = ctx.measureText(txt);
    return m.width;
}

function sound(src) {
    this.sound = document.createElement('audio');
    this.sound.src = src;
    this.sound.setAttribute('preload', 'auto');
    this.sound.setAttribute('controls', 'none');
    this.sound.style.display = 'none';
    document.body.appendChild(this.sound);
    this.play = function () {
        this.sound.play();
    }
    this.stop = function () {
        this.sound.pause();
    }
}